import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import type { BSPaperItem } from '@/types/BSPaperItem'
import type { BSPaper } from '@/types/BSPaper'
import { useAuthStore } from './auth'
import itembsService from '@/services/itembs'
import { useLoadingStore } from './loading'
import { useMessageStore } from './message'

export const useBSPaperStore = defineStore('bspaper', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const authStore = useAuthStore()
  const DeleteDialog = ref(false)
  const bsPaperItems = ref<BSPaperItem[]>([])
  const bsPaper = ref<BSPaper>({
    id: 0,
    createdDate: new Date(),
    total: 0,
    amount: 0,
    userId: authStore.getCurrentUser()!.id!,
    user: authStore.getCurrentUser()!
  })
  initBSPaper()
  function initBSPaper() {
    bsPaper.value = {
      id: 0,
      createdDate: new Date(),
      total: 0,
      amount: 0,
      userId: authStore.getCurrentUser()!.id!,
      user: authStore.getCurrentUser()!
    }
    bsPaperItems.value = []
  }

  watch(
    bsPaperItems,
    () => {
      calBSPaper()
    },
    { deep: true }
  )

  const calBSPaper = function () {
    bsPaper.value!.total = 0
    bsPaper.value!.amount = 0
    for (let i = 0; i < bsPaperItems.value.length; i++) {
      bsPaper.value!.total += bsPaperItems.value[i].price * bsPaperItems.value[i].qty
      bsPaper.value!.amount += bsPaperItems.value[i].qty
    }
  }

  const addBSPaperItem = (newBSPaperItem: BSPaperItem) => {
    bsPaperItems.value.push(newBSPaperItem)
  }
  const deleteBSPaperItem = (selectedItem: BSPaperItem) => {
    const index = bsPaperItems.value.findIndex((item) => item === selectedItem)
    bsPaperItems.value.splice(index, 1)
  }
  const incQtyOfBSPaperItem = (selectedItem: BSPaperItem) => {
    selectedItem.qty++
  }
  const decQtyOfBSPaperItem = (selectedItem: BSPaperItem) => {
    selectedItem.qty--
    if (selectedItem.qty === 0) {
      deleteBSPaperItem(selectedItem)
    }
  }
  const removeItem = (item: BSPaperItem) => {
    const index = bsPaperItems.value.findIndex((ri) => ri === item)
    bsPaperItems.value.splice(index, 1)
  }

  function clearForm() {
    bsPaper.value = {
      id: 0,
      createdDate: new Date(),
      total: 0,
      amount: 0,
      userId: authStore.getCurrentUser()!.id!,
      user: authStore.getCurrentUser()!
    }
    bsPaperItems.value = []
  }

  function showDeleteDialog() {
    bsPaper.value.buystockItems = bsPaperItems.value
    DeleteDialog.value = true
  }

  const Buystock = async () => {
    try {
      loadingStore.doLoad()
      await itembsService.addItemBS(bsPaper.value!, bsPaperItems.value)
      initBSPaper()
      loadingStore.finish()
    } catch (e: any) {
      loadingStore.finish()
      messageStore.showMessage(e.message)
    }
  }

  return {
    bsPaper,
    DeleteDialog,
    initBSPaper,
    bsPaperItems,
    addBSPaperItem,
    incQtyOfBSPaperItem,
    decQtyOfBSPaperItem,
    deleteBSPaperItem,
    removeItem,
    Buystock,
    clearForm,
    showDeleteDialog
  }
})
