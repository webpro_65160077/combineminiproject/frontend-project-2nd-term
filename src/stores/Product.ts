import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Product } from '@/types/Product'

export const useproductStore = defineStore('product', () => {
  const products: Product[] = [
    { id: 1, name: 'คาปูชิโน่', price: 50.0, type: 1 },
    { id: 2, name: 'เอสเปรสโซ่', price: 45.0, type: 1 },
    { id: 3, name: 'ลาเต้ ', price: 50.0, type: 1 },
    { id: 4, name: 'ชาเขียวมัทฉะ', price: 55.0, type: 1 },
    { id: 5, name: 'ชาไทย', price: 50.0, type: 1 },
    { id: 6, name: 'ช็อคโกแลตร้อน', price: 50.0, type: 1 },
    { id: 7, name: 'โกโก้', price: 50.0, type: 1 },
    { id: 8, name: 'คาราเมลมัคคิอาโต้', price: 55.0, type: 1 },
    { id: 9, name: 'นมร้อน', price: 25.0, type: 1 },
    { id: 10, name: 'ชาดำ', price: 35.0, type: 1 },
    { id: 11, name: 'กาแฟส้ม', price: 45.0, type: 1 },
    { id: 12, name: 'เค้กช็อคโกแลต', price: 35.0, type: 2 },
    { id: 13, name: 'สตอเบอรี่ช็อตเค้ก ', price: 45.0, type: 2 },
    { id: 14, name: 'เค้กชาเขียว', price: 35.0, type: 2 },
    { id: 15, name: 'เค้กกาแฟ ', price: 35.0, type: 2 },
    { id: 16, name: 'เค้กส้ม', price: 40.0, type: 2 },
    { id: 17, name: 'เครปเค้ก', price: 45.0, type: 2 },
    { id: 18, name: 'ชูครีมวนิลานมสด', price: 45.0, type: 2 },
    { id: 19, name: 'ครัวซองค์', price: 30.0, type: 2 },
    { id: 20, name: 'สปาเก็ตตี้คาโบนาร่า', price: 79.0, type: 3 },
    { id: 21, name: 'สเต๊กไก่ ', price: 89.0, type: 3 },
    { id: 22, name: 'ข้าวซอยไก่', price: 69.0, type: 3 },
    { id: 23, name: 'ข้าวกระเพราหมูสับ ', price: 59.0, type: 3 },
    { id: 24, name: 'ข้าวผัดกุนเชียง', price: 59.0, type: 3 },
    { id: 25, name: 'ผัดซีอิ๊ว หมู', price: 59.0, type: 3 },
    { id: 26, name: 'ต้มยำกุ้ง น้ำข้น', price: 129.0, type: 3 },
    { id: 27, name: 'แกงเขียวหวาน', price: 99.0, type: 3 },
    { id: 28, name: 'ไข่เจียวหมูสับ', price: 59.0, type: 3 },
    { id: 29, name: 'ไข่กระทะ', price: 49.0, type: 3 },
    { id: 30, name: 'ข้าวสวย', price: 20.0, type: 3 }
  ]

  const searchProduct = (productId: number): Product | null => {
    if (productId < 0) {
      return null
    }

    const foundProduct = products.find((product) => product.id === productId)
    return foundProduct || null
  }

  return { products, searchProduct }
})
