import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { Member } from '@/types/Member'
import { useReceiptStore } from './receipt'
import memberService from '@/services/member'
import { useLoadingStore } from './loading'
export const useMemberStore = defineStore('member', () => {
  const receiptStore = useReceiptStore()
  const tel = ref('')
  const addNewMemberDialog = ref(false)
  const useMemberDialog = ref(false)
  const dialogConfirm = ref(false)
  const members = ref<Member[]>([])
  const loadingStore = useLoadingStore()
  const initiaMember: Member = {
    name: '',
    tel: '',
    point: 0
  }
  const editedMember = ref<Member>(JSON.parse(JSON.stringify(initiaMember)))

  const currentMember = ref<Member | null>(null)
  // behind the parameter in of function is what it will return
  const searchMember = (tel: string) => {
    const index = members.value.findIndex((item) => item.tel === tel)
    if (index < 0) {
      currentMember.value = null
    }
    currentMember.value = members.value[index]
  }

  const clear = () => {
    editedMember.value = JSON.parse(JSON.stringify(initiaMember))
  }
  const clearTextField = () => {
    if (tel.value !== undefined && tel.value !== null) {
      tel.value = '' // this value is holding input
    }
    console.log(tel.value)
  }
  const cancel = () => {
    addNewMemberDialog.value = false
  }

  async function getMember(id: number) {
    loadingStore.doLoad()
    const res = await memberService.getMember(id)
    editedMember.value = res.data
    loadingStore.finish()
  }

  async function getMembers() {
    loadingStore.doLoad()
    const res = await memberService.getMembers()
    members.value = res.data
    loadingStore.finish()
  }

  async function saveMember() {
    loadingStore.doLoad()
    const member = editedMember.value
    if (!member.id) {
      // Add new
      console.log('Post' + JSON.stringify(member))
      const res = await memberService.addMember(member)
    } else {
      // Update
      console.log('Patch' + JSON.stringify(member))
      const res = await memberService.updateMember(member)
    }

    await getMembers()
    loadingStore.finish()
  }

  async function deleteMember() {
    loadingStore.doLoad()
    const member = editedMember.value
    const res = await memberService.delMember(member)

    await getMembers()
    loadingStore.finish()
  }

  const logCheckCurrentMember = () => {
    console.log('this member id is ' + String(currentMember.value?.id))
  }

  const alert = ref(false)
  const alertMessage = ref('')
  const alertType = ref<'success' | 'error' | 'warning' | 'info' | undefined>('success')

  const showAlert = (
    message: string,
    type: 'success' | 'error' | 'warning' | 'info' | undefined
  ) => {
    alertMessage.value = message
    alertType.value = type
    alert.value = true
    setTimeout(() => {
      alert.value = false
    }, 3000) // Adjust the timeout as needed
  }

  const usePoint = () => {
    // Check if there is a current member
    if (currentMember.value && currentMember.value !== null) {
      // Check if the member has at least 10 points
      if (currentMember.value?.point && currentMember.value?.point >= 10) {
        // Apply the discount and deduct 10 points
        receiptStore.receipt.memberDiscount = 50
        currentMember.value.point -= 10
        console.log('Used Point!')
        showAlert('Points used successfully!', 'success')
      } else {
        // Log a message if the member has fewer than 10 points
        console.log('Not enough points to use for a discount.')
        showAlert('Not enough points to use for a discount.', 'error')
      }
    } else {
      // Log a message if there is no current member
      console.log('No current member selected.')
      showAlert('No current member selected.', 'info')
    }
    console.log(currentMember)
  }

  return {
    members,
    currentMember,
    tel,
    editedMember,
    addNewMemberDialog,
    useMemberDialog,
    dialogConfirm,
    alert,
    alertMessage,
    alertType,
    searchMember,
    clear,
    clearTextField,
    cancel,
    saveMember,
    logCheckCurrentMember,
    usePoint,
    showAlert,
    getMember,
    deleteMember,
    getMembers
  }
})
