import { defineStore } from 'pinia'
import { ref } from 'vue'
import { useRouter } from 'vue-router'
import { useLoadingStore } from './loading'
import type { Branch } from '@/types/Branch'
import branchService from '@/services/branch'
export const useBranchStore = defineStore('branch', () => {
  const branchs = ref<Branch[]>([])

  const initialBranch: Branch = {
    name: '',
    maneger: '',
    address: '',
    tels: ''
  }
  const editedBranch = ref<Branch>(JSON.parse(JSON.stringify(initialBranch)))

  const branchname = ref('')
  const password = ref('')
  const router = useRouter()
  const dialogFailed = ref(false)
  const isLoggedIn = ref(false)
  const loadingStore = useLoadingStore()

  function closeDialog() {
    dialogFailed.value = false
  }

  async function getBranch(id: number) {
    loadingStore.doLoad()
    const res = await branchService.getBranch(id)
    editedBranch.value = res.data
    loadingStore.finish()
  }

  async function getBranchs() {
    loadingStore.doLoad()
    const res = await branchService.getBranchs()
    branchs.value = res.data
    loadingStore.finish()
  }

  async function saveBranch() {
    loadingStore.doLoad()
    const branch = editedBranch.value
    if (!branch.id) {
      // Add new
      console.log('Post' + JSON.stringify(branch))
      const res = await branchService.addBranch(branch)
    } else {
      // Update
      console.log('Patch' + JSON.stringify(branch))
      const res = await branchService.updateBranch(branch)
    }

    await getBranchs()
    loadingStore.finish()
  }

  async function deleteBranch() {
    loadingStore.doLoad()
    const branch = editedBranch.value
    const res = await branchService.delBranch(branch)

    await getBranchs()
    loadingStore.finish()
  }

  function clearForm() {
    editedBranch.value = JSON.parse(JSON.stringify(initialBranch))
  }

  return {
    branchs,
    branchname,
    password,
    dialogFailed,
    closeDialog,
    isLoggedIn,
    getBranchs,
    saveBranch,
    deleteBranch,
    editedBranch,
    getBranch,
    clearForm
  }
})
