import type { Promotion } from '@/types/Promotion'
import { defineStore } from 'pinia'
import { computed, ref, watch } from 'vue'
import { useReceiptStore } from './receipt'
import POSViewVue from '@/views/POS/POSView.vue'
import type { Product } from '@/types/Product'
import { useproductStore } from './Product'
import { type ReceiptItem } from '@/types/ReceiptItem'
import { useLoadingStore } from './loading'
import promotionService from '@/services/promotion'

export const usePromotionStore = defineStore('Promotion', () => {
  const productStore = useproductStore()
  const receiptStore = useReceiptStore()
  const loadingStore = useLoadingStore()
  // Date picker

  const promotionConfirmDialog = ref(false)
  const promotionConditonCardDialog = ref(false)

  //ไว้กำหนดให้ซ่อนหรือให้เห็น
  const showPromotiondialog = ref(false)
  const promotionDialog = ref(true)
  const initialPromotion: Promotion = {
    code: '',
    name: '',
    startdate: '',
    enddate: '',
    status: 'disable',
    promotionItems: [
      {
        productId: null,
        product: undefined,
        conditionQty: null,
        conditionPrice: null,
        discountPercent: null,
        discountPrice: null,
        status: null
      }
    ]
  }

  const editedPromotion = ref<Promotion>(JSON.parse(JSON.stringify(initialPromotion)))
  function clearForm() {
    clearpromotionItemselectedChoice()
    editedPromotion.value = JSON.parse(JSON.stringify(initialPromotion))
    console.log('editedPromotion.value.promotionItems =', editedPromotion.value.promotionItems)
  }

  async function delPromotion() {
    try {
      loadingStore.doLoad()
      const promotion = editedPromotion.value
      const res = await promotionService.delPromotion(promotion)

      await getPromotions()
      loadingStore.finish()
    } catch (error) {
      await getPromotions()
      loadingStore.finish()
    }
  }

  async function getPromotion(id: number) {
    try {
      loadingStore.doLoad()
      const res = await promotionService.getPromotion(id)
      editedPromotion.value = res.data
      loadingStore.finish()
    } catch (error) {
      console.log(error)
      loadingStore.finish()
    }
  }

  async function getPromotions() {
    try {
      loadingStore.doLoad()
      const res = await promotionService.getPromotions()
      const promotionsData = res.data

      promotionsData.forEach((promotion: Promotion) => {
        promotion.status = getStatus(promotion.startdate, promotion.enddate)
      })

      promotions.value = promotionsData // Assuming promotions is a ref() or reactive() state variable
      loadingStore.finish()
    } catch (error) {
      console.error(error)
      loadingStore.finish()
    }
  }

  async function savePromotion() {
    try {
      loadingStore.doLoad()
      // editedPromotion.value.startdate = startDate.value
      // editedPromotion.value.enddate = endDate.value
      const promotion = editedPromotion.value
      if (!promotion.id) {
        // Add new
        console.log('Create<Post>' + JSON.stringify(promotion))
        const res = await promotionService.addPromotion(promotion)
      } else {
        // Update
        console.log('Update<Post>' + JSON.stringify(promotion))
        const res = await promotionService.updatePromotion(promotion)
      }
      await getPromotions()
      loadingStore.finish()
    } catch (error) {
      console.log(error)
      loadingStore.finish()
    }
  }
  const getStatus = (
    startDate: string | null | undefined,
    endDate: string | null | undefined
  ): 'enable' | 'disable' => {
    if (!startDate || !endDate) {
      return 'disable'
    }

    const currentDate = new Date()
    const [day, month, year] = [
      currentDate.getDate(),
      currentDate.getMonth() + 1,
      currentDate.getFullYear()
    ]

    // Manually construct the start and end date objects
    const [startDay, startMonth, startYear] = startDate.split('-').map(Number)
    const start = new Date(startYear, startMonth - 1, startDay) // month is zero-based

    const [endDay, endMonth, endYear] = endDate.split('-').map(Number)
    const end = new Date(endYear, endMonth - 1, endDay) // month is zero-based

    // Check if current date is within the range
    if (currentDate >= start && currentDate <= end) {
      return 'enable'
    } else {
      return 'disable'
    }
  }

  const promotions = ref<Promotion[]>([])

  // this is about search things

  const searchQuery = ref('')
  // chat gpt mod this to sort kub
  // use this to search in the seacrhbar
  const searchfilteredPromotions = computed(() => {
    const filtered = promotions.value.filter((promotion: Promotion) =>
      promotion.name.toLowerCase().includes(searchQuery.value.toLowerCase())
    )

    // Custom sorting function
    filtered.sort((a, b) => {
      // 'enable' promotions come first
      if (a.status === 'enable' && b.status !== 'enable') {
        return -1
      } else if (a.status !== 'enable' && b.status === 'enable') {
        return 1
      }

      // Sort by other criteria (you can adjust this based on your needs)
      // For example, sorting by start date
      const dateA = new Date(a.startdate).getTime()
      const dateB = new Date(b.startdate).getTime()

      return dateA - dateB
    })

    return filtered
  })

  //select promotion
  const selectedPromotionId = ref<number>()
  const selectedPromotion = computed(() => {
    if (selectedPromotionId.value !== null) {
      // Use the index to find the promotion in the promotions array
      const editedIndex = promotions.value.findIndex(
        (item) => item.id === selectedPromotionId.value
      )
      return promotions.value[editedIndex] || null
    }
    return null
  })

  const disUsePromotion = (promotion: Promotion) => {
    if (
      promotion.promotionItems![0].productId === null &&
      promotion.promotionItems![0].discountPercent !== null
    ) {
      disusePromotionNoProductIdDiscountPercent(promotion)
    } else if (
      promotion.promotionItems![0].productId === null &&
      promotion.promotionItems![0].discountPrice !== null
    ) {
      disusePromotionNoProductIdDiscountPrice(promotion)
    } else if (
      promotion.promotionItems![0].productId !== null &&
      promotion.promotionItems![0].discountPrice !== null
    ) {
      disusePromotionHaveProductIdDiscountPrice(promotion)
    } else if (
      promotion.promotionItems![0].productId !== null &&
      promotion.promotionItems![0].conditionQty !== null &&
      promotion.promotionItems![0].discountPercent !== null
    ) {
      disusePromotionHaveProductIdConditionQtyDiscountPercent(promotion)
    }
    if (
      promotion.promotionItems![0].productId !== null &&
      promotion.promotionItems![0].conditionPrice !== null &&
      promotion.promotionItems![0].discountPercent !== null
    ) {
      disusePromotionHaveProductIdConditionPriceDiscountPercent(promotion)
    }
  }
  const disusePromotionNoProductIdDiscountPercent = (promotion: Promotion) => {
    if (
      promotion.promotionItems![0].productId === null &&
      promotion.promotionItems![0].discountPercent !== null
    ) {
      const cancel =
        (promotion.promotionItems![0].discountPercent / 100) * receiptStore.receipt.totalBefore
      receiptStore.receipt.promotionDiscount = receiptStore.receipt.promotionDiscount - cancel
    }
  }
  const disusePromotionNoProductIdDiscountPrice = (promotion: Promotion) => {
    if (
      promotion.promotionItems![0].productId === null &&
      promotion.promotionItems![0].discountPrice !== null
    ) {
      receiptStore.receipt.promotionDiscount =
        receiptStore.receipt.promotionDiscount - promotion.promotionItems![0].discountPrice
    }
  }

  const disusePromotionHaveProductIdDiscountPrice = (promotion: Promotion) => {
    if (
      promotion.promotionItems![0].productId !== null &&
      promotion.promotionItems![0].discountPrice !== null
    ) {
      receiptStore.receipt.promotionDiscount =
        receiptStore.receipt.promotionDiscount - promotion.promotionItems![0].discountPrice
    }
  }
  const disusePromotionHaveProductIdConditionQtyDiscountPercent = (promotion: Promotion) => {
    if (
      promotion.promotionItems![0].productId !== null &&
      promotion.promotionItems![0].conditionQty !== null &&
      promotion.promotionItems![0].discountPercent !== null
    ) {
      const findresult = receiptStore.receiptItems.find(
        (receiptItem) => receiptItem.productId === promotion.promotionItems![0].productId
      )
      const disCountPrice =
        promotion.promotionItems![0].conditionQty *
        findresult?.price! *
        (promotion.promotionItems![0].discountPercent / 100)

      receiptStore.receipt.promotionDiscount =
        receiptStore.receipt.promotionDiscount - disCountPrice
    }
  }
  const disusePromotionHaveProductIdConditionPriceDiscountPercent = (promotion: Promotion) => {
    if (
      promotion.promotionItems![0].productId !== null &&
      promotion.promotionItems![0].conditionPrice !== null &&
      promotion.promotionItems![0].discountPercent !== null
    ) {
      const findresult = receiptStore.receiptItems.find(
        (receiptItem) => receiptItem.productId === promotion.promotionItems![0].productId
      )
      const disCountPrice =
        promotion.promotionItems![0].conditionPrice! *
        (promotion.promotionItems![0].discountPercent / 100)

      receiptStore.receipt.promotionDiscount =
        receiptStore.receipt.promotionDiscount - disCountPrice
    }
  }

  const usePromotion = (promotion: Promotion) => {
    if (
      promotion.promotionItems![0].productId === null &&
      promotion.promotionItems![0].discountPercent !== null
    ) {
      usePromotionNoProductIdDiscountPercent(promotion)
    } else if (
      promotion.promotionItems![0].productId === null &&
      promotion.promotionItems![0].discountPrice !== null
    ) {
      usePromotionNoProductIdDiscountPrice(promotion)
    } else if (
      promotion.promotionItems![0].productId !== null &&
      promotion.promotionItems![0].discountPrice !== null
    ) {
      usePromotionHaveProductIdDiscountPrice(promotion)
    } else if (
      promotion.promotionItems![0].productId !== null &&
      promotion.promotionItems![0].conditionQty !== null &&
      promotion.promotionItems![0].discountPercent !== null
    ) {
      usePromotionHaveProductIdConditionQtyDiscountPercent(promotion)
    } else if (
      promotion.promotionItems![0].productId !== null &&
      promotion.promotionItems![0].conditionPrice !== null &&
      promotion.promotionItems![0].discountPercent !== null
    ) {
      usePromotionHaveProductIdConditionPriceDiscountPercent(promotion)
    }
  }

  const usePromotionNoProductIdDiscountPercent = (promotion: Promotion) => {
    if (
      promotion.promotionItems![0].productId === null &&
      promotion.promotionItems![0].discountPercent !== null
    ) {
      receiptStore.receipt.promotionDiscount =
        (promotion.promotionItems![0].discountPercent / 100) * receiptStore.receipt.totalBefore +
        receiptStore.receipt.promotionDiscount
    }
  }

  const usePromotionNoProductIdDiscountPrice = (promotion: Promotion) => {
    if (
      promotion.promotionItems![0].productId === null &&
      promotion.promotionItems![0].discountPrice !== null
    ) {
      receiptStore.receipt.promotionDiscount =
        promotion.promotionItems![0].discountPrice + receiptStore.receipt.promotionDiscount
    }
  }

  const usePromotionHaveProductIdConditionQtyDiscountPercent = (promotion: Promotion) => {
    if (
      promotion.promotionItems![0].productId !== null &&
      promotion.promotionItems![0].conditionQty !== null &&
      promotion.promotionItems![0].discountPercent !== null
    ) {
      const findresult = receiptStore.receiptItems.find(
        (receiptItem) => receiptItem.productId === promotion.promotionItems![0].productId
      )
      const disCountPrice =
        promotion.promotionItems![0].conditionQty *
        findresult?.price! *
        (promotion.promotionItems![0].discountPercent / 100)

      receiptStore.receipt.promotionDiscount =
        disCountPrice + receiptStore.receipt.promotionDiscount
    }
  }

  const usePromotionHaveProductIdConditionPriceDiscountPercent = (promotion: Promotion) => {
    if (
      promotion.promotionItems![0].productId !== null &&
      promotion.promotionItems![0].conditionPrice !== null &&
      promotion.promotionItems![0].discountPercent !== null
    ) {
      const findresult = receiptStore.receiptItems.find(
        (receiptItem) => receiptItem.productId === promotion.promotionItems![0].productId
      )
      const disCountPrice =
        promotion.promotionItems![0].conditionPrice! *
        (promotion.promotionItems![0].discountPercent / 100)

      receiptStore.receipt.promotionDiscount =
        disCountPrice + receiptStore.receipt.promotionDiscount
    }
  }

  const usePromotionHaveProductIdDiscountPrice = (promotion: Promotion) => {
    if (
      promotion.promotionItems![0].productId !== null &&
      promotion.promotionItems![0].discountPrice !== null
    ) {
      receiptStore.receipt.promotionDiscount =
        promotion.promotionItems![0].discountPrice + receiptStore.receipt.promotionDiscount
    }
  }

  const promotionItemtype = ref([
    'ไม่มีสินค้าซื้อตามจำนวนลดด้วยเปอร์เซ็น',
    'ไม่มีสินค้าซื้อตามจำนวนลดด้วยราคาเจาะจง',
    'ไม่มีสินค้าซื้อตามราคาลดด้วยเปอร์เซ็น',
    'ไม่มีสินค้าซื้อตามราคาลดด้วยราคาเจาะจง',
    'มีสินค้าซื้อตามจำนวนลดด้วยเปอร์เซ็น',
    'มีสินค้าซื้อตามจำนวนลดด้วยราคาเจาะจง',
    'มีสินค้าซื้อตามราคาลดด้วยเปอร์เซ็น',
    'มีสินค้าซื้อตามราคาลดด้วยราคาเจาะจง'
  ])

  const promotionItemselectedChoice = ref('กรุณาเลือกชนิดของPromotion')

  const clearpromotionItemselectedChoice = () => {
    promotionItemselectedChoice.value = 'กรุณาเลือกชนิดของPromotion'
  }

  // watch อันนี้มี ไว้เพื่อเวลาpromotionItemselectedChoiceถูกเปลี่ยน ให้setค่า edit Promotion เป็น ค่าเริ่มต้น
  watch(promotionItemselectedChoice, (newValue, oldValue) => {
    // console.log(`promotionItemselectedChoice changed from ${oldValue} to ${newValue}`)
    editedPromotion.value.promotionItems![0] = initialPromotion.promotionItems![0]
  })

  const shouldRenderProductIdtextfield = computed(() => {
    if (promotionItemselectedChoice.value !== null) {
      if (promotionItemselectedChoice.value.includes('ไม่มีสินค้า')) {
        return false
      } else if (promotionItemselectedChoice.value.includes('มีสินค้า')) {
        return true
      } else {
        return false
      }
    } else {
      return false
    }
  })
  const shouldRenderConPriceTextfield = computed(() => {
    // Check if promotionItemselectedChoice.value is not null and includes 'ซื้อตามราคา'
    if (promotionItemselectedChoice.value !== null) {
      if (promotionItemselectedChoice.value.includes('ซื้อตามราคา')) {
        return true
      }
    }
    return false
  })
  const shouldRenderConQuantityTextfield = computed(() => {
    // Check if promotionItemselectedChoice.value is not null and includes 'ซื้อตามราคา'
    if (promotionItemselectedChoice.value !== null) {
      if (promotionItemselectedChoice.value.includes('ซื้อตามจำนวน')) {
        return true
      }
    }
    return false
  })
  const shouldRenderDisPriceTextfield = computed(() => {
    // Check if promotionItemselectedChoice.value is not null and includes 'ลดด้วยราคาเจาะจง'
    if (promotionItemselectedChoice.value !== null) {
      if (promotionItemselectedChoice.value.includes('ลดด้วยราคาเจาะจง')) {
        return true
      }
    }
    return false
  })
  const shouldRenderDisPercentTextfield = computed(() => {
    // Check if promotionItemselectedChoice.value is not null and includes 'ซื้อตามราคา'
    if (promotionItemselectedChoice.value !== null) {
      if (promotionItemselectedChoice.value.includes('ลดด้วยเปอร์เซ็น')) {
        return true
      }
    }
    return false
  })

  return {
    promotionDialog,
    showPromotiondialog,
    promotions,
    promotionConfirmDialog,
    usePromotion,
    disUsePromotion,
    getStatus,
    promotionConditonCardDialog,
    searchfilteredPromotions,
    searchQuery,
    selectedPromotionId,
    selectedPromotion,
    clearForm,
    delPromotion,
    getPromotion,
    getPromotions,
    savePromotion,
    editedPromotion,
    promotionItemtype,
    promotionItemselectedChoice,
    shouldRenderProductIdtextfield,
    shouldRenderConPriceTextfield,
    shouldRenderConQuantityTextfield,
    shouldRenderDisPriceTextfield,
    shouldRenderDisPercentTextfield
  }
})
