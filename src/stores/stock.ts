import type { Stock } from '@/types/Stock'
import { defineStore } from 'pinia'
import { computed, ref } from 'vue'
import { useRouter } from 'vue-router'
import { useLoadingStore } from './loading'
import stockService from '@/services/stock'

export const useStockStore = defineStore('stock', () => {
  const loadingStore = useLoadingStore()
  const material = ref<Stock[]>([])
  const search = ref<string>('')
  const initialStock: Stock & { files: File[] } = {
    name: '',
    status: 'Available',
    min: 0,
    balance: 0,
    unit: '',
    image: 'noimage.jpg',
    branch: { id: 1, name: 'b1', maneger: 'wit', address: '52', tels: '092' },
    files: []
  }
  const editedStock = ref<Stock & { files: File[] }>(JSON.parse(JSON.stringify(initialStock)))

  async function getStocks() {
    try {
      loadingStore.doLoad()
      const res = await stockService.getStocks()
      material.value = res.data
      loadingStore.finish()
    } catch (error) {
      // Handle error appropriately, e.g., log it or show an error message to the user
      console.error('Error fetching stocks:', error)
      loadingStore.finish() // Make sure to finish loading even if an error occurs
    }
  }

  async function getStock(id: number) {
    try {
      loadingStore.doLoad()
      const res = await stockService.getStock(id)
      editedStock.value = res.data
      loadingStore.finish()
    } catch (e: any) {
      console.log('Error')
      loadingStore.finish()
    }
  }

  async function saveStock() {
    try {
      loadingStore.doLoad()
      const stock = editedStock.value
      if (!stock.id) {
        console.log('Post' + JSON.stringify(stock))
        const res = await stockService.addStock(stock)
      } else {
        console.log('Patch' + JSON.stringify(stock))
        const res = await stockService.updateStock(stock)
      }
      await getStocks()
      loadingStore.finish()
    } catch (e: any) {
      console.log('Error')
      loadingStore.finish()
    }
  }

  async function deleteStock() {
    loadingStore.doLoad()
    const stock = editedStock.value
    const res = await stockService.delStock(stock)
    await getStocks()
    loadingStore.finish()
  }

  const searchStock = computed(() => {
    const searchTerm = search.value.toLowerCase()
    return material.value.filter((item) => {
      return (
        item.name.toLowerCase().includes(searchTerm) ||
        item.status.toLowerCase().includes(searchTerm)
      )
    })
  })

  const totalStocks = computed(() => material.value.length)
  const totalAvailableStocks = computed(() => {
    return searchStock.value.filter((item) => item.status === 'Available').length
  })
  const totalLowStocks = computed(() => {
    return searchStock.value.filter((item) => item.status === 'Low').length
  })
  const totalOutStocks = computed(() => {
    return searchStock.value.filter((item) => item.status === 'Out of Stock').length
  })

  const availableStocks = computed(() => {
    return searchStock.value.filter((item) => item.status === 'Available')
  })
  const lowStocks = computed(() => {
    return searchStock.value.filter((item) => item.status === 'Low')
  })
  const outStocks = computed(() => {
    return searchStock.value.filter((item) => item.status === 'Out of Stock')
  })
  function clearForm() {
    editedStock.value = JSON.parse(JSON.stringify(initialStock))
  }

  return {
    getStocks,
    getStock,
    saveStock,
    deleteStock,
    search,
    totalStocks,
    totalAvailableStocks,
    totalLowStocks,
    totalOutStocks,
    availableStocks,
    lowStocks,
    outStocks,
    searchStock,
    editedStock,
    clearForm
  }
})
