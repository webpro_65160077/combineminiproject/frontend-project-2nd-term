import { defineStore } from 'pinia'
import { ref } from 'vue'
import { useRouter } from 'vue-router'
import { useLoadingStore } from './loading'
import type { Customer } from '@/types/Customer'
import customerService from '@/services/customer'
export const useCustomerStore = defineStore('customer', () => {
  const customers = ref<Customer[]>([])

  const initialCustomer: Customer = {
    name: '',
    tel: '',
    point: 0
  }
  const editedCustomer = ref<Customer>(JSON.parse(JSON.stringify(initialCustomer)))

  const customername = ref('')
  const password = ref('')
  const router = useRouter()
  const dialogFailed = ref(false)
  const isLoggedIn = ref(false)
  const loadingStore = useLoadingStore()

  function closeDialog() {
    dialogFailed.value = false
  }

  async function getCustomer(id: number) {
    loadingStore.doLoad()
    const res = await customerService.getCustomer(id)
    editedCustomer.value = res.data
    loadingStore.finish()
  }

  async function getCustomers() {
    loadingStore.doLoad()
    const res = await customerService.getCustomers()
    customers.value = res.data
    loadingStore.finish()
  }

  async function saveCustomer() {
    loadingStore.doLoad()
    const customer = editedCustomer.value
    if (!customer.id) {
      // Add new
      console.log('Post' + JSON.stringify(customer))
      const res = await customerService.addCustomer(customer)
    } else {
      // Update
      console.log('Patch' + JSON.stringify(customer))
      const res = await customerService.updateCustomer(customer)
    }

    await getCustomers()
    loadingStore.finish()
  }

  async function deleteCustomer() {
    loadingStore.doLoad()
    const customer = editedCustomer.value
    const res = await customerService.delCustomer(customer)

    await getCustomers()
    loadingStore.finish()
  }

  function clearForm() {
    editedCustomer.value = JSON.parse(JSON.stringify(initialCustomer))
  }

  return {
    customers,
    customername,
    password,
    dialogFailed,
    closeDialog,
    isLoggedIn,
    getCustomers,
    saveCustomer,
    deleteCustomer,
    editedCustomer,
    getCustomer,
    clearForm
  }
})
