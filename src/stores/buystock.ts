import type { BuyStock } from '@/types/BuyStock'
import { defineStore } from 'pinia'
import { computed, ref } from 'vue'
import { useRouter } from 'vue-router'
import { useLoadingStore } from './loading'
import buyStockService from '@/services/buyStock'
import { useMessageStore } from './message'

export const useBuyStockStore = defineStore('buyStock', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const buyStocks = ref<BuyStock[]>([])
  const initialBuyStock: BuyStock = {
    name: '',
    price: 0,
    unit: ''
  }
  const editedBuyStock = ref<BuyStock>(JSON.parse(JSON.stringify(initialBuyStock)))

  async function getBuyStock(id: number) {
    try {
      loadingStore.doLoad()
      const res = await buyStockService.getBuyStock(id)
      editedBuyStock.value = res.data
      loadingStore.finish()
    } catch (e: any) {
      loadingStore.finish()
      messageStore.showMessage(e.message)
    }
  }
  async function getBuyStocks() {
    try {
      loadingStore.doLoad()
      const res = await buyStockService.getBuyStocks()
      buyStocks.value = res.data
      loadingStore.finish()
    } catch (e) {
      console.log('Error')
      loadingStore.finish()
    }
  }
  async function saveBuyStock() {
    try {
      loadingStore.doLoad()
      const buyStock = editedBuyStock.value
      if (!buyStock.id) {
        // Add new
        console.log('Post ' + JSON.stringify(buyStock))
        const res = await buyStockService.addBuyStock(buyStock)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(buyStock))
        const res = await buyStockService.updateBuyStock(buyStock)
      }
      await getBuyStocks()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }
  async function deleteBuyStock() {
    loadingStore.doLoad()
    const buyStock = editedBuyStock.value
    const res = await buyStockService.delBuyStock(buyStock)

    await getBuyStocks()
    loadingStore.finish()
  }
  function clearForm() {
    editedBuyStock.value = JSON.parse(JSON.stringify(initialBuyStock))
  }
  return {
    buyStocks,
    getBuyStocks,
    saveBuyStock,
    deleteBuyStock,
    editedBuyStock,
    getBuyStock,
    clearForm
  }
})
