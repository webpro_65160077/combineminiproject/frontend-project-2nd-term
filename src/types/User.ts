type Gender = 'male' | 'female' | 'others'
type Role = 'user' | 'admin'

type User = {
  id?: number
  email: string
  password: string
  fullName: string
  gender: Gender //Male, Female, Others
  roles: Role[] // admin,user
}
export type { Gender, Role, User }
