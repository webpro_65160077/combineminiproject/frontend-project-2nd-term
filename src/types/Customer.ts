type Customer = {
  id?: number
  name: string
  tel: string
  point: number
}

export type { Customer }
