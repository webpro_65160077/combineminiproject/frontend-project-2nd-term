import { usePromotionStore } from '@/stores/Promotion'
import type { PromotionItem } from './PromotionItem'
const promotionStore = usePromotionStore()
type Promotion = {
  id?: number
  code: string
  name: string
  startdate: string
  enddate: string
  status: 'enable' | 'disable'
  promotionItems?: PromotionItem[] | undefined
}

export { type Promotion }
