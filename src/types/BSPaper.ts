import type { BSPaperItem } from './BSPaperItem'
import type { User } from './User'

const defaultBSPaper = {
  id: -1,
  createdDate: new Date(),
  total: 0,
  paymentType: 'cash',
  employeeId: 1
}
type BSPaper = {
  id?: number
  createdDate: Date
  total: number
  amount: number
  userId: number
  buystockItems?: BSPaperItem[]
  user?: User
}

export { type BSPaper }
