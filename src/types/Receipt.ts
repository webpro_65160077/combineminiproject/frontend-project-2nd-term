import type { Member } from './Member'
import type { ReceiptItem } from './ReceiptItem'
import type { User } from './User'

type Receipt = {
  id: number
  createdDate: Date
  totalBefore: number
  memberDiscount: number
  promotionDiscount: number
  totalDiscount: number
  total: number
  receivedAmount: number
  change: number
  paymentType: string
  userId?: number
  user?: User | null
  memberId: number
  member?: Member
  receiptItems?: ReceiptItem[]
}

export type { Receipt }
