import type { Product } from './Product'

// something that is type of variable should keep in here
type ReceiptItem = {
  id: number
  name: string
  price: number
  unit: number
  productId: number
  // the question mark thing shows that it is optional to have
  product?: Product
}

export { type ReceiptItem }
